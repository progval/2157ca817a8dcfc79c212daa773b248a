#!/bin/sh
NUM=$1
PORT=$2
MYSQL_HOST=${MYSQL_HOST:-192.168.2.1}
DIR=/home/official-infclass/teeworlds-infclass
FNCFG=$DIR/config/tw06-infclass$NUM.cfg
FNGDB=$DIR/config/tw06-infclass$NUM.gdb
FNLOG=$DIR/logs/tw06-infclass$NUM-$(date +"%y-%m-%d-%T").txt
PARAM="-f $FNCFG"

cd $DIR

echo "Server number: $NUM"
echo "Config file: $FNCFG"
echo "GDB Config file: $FNGDB"
echo "Log file: $FNLOG"
echo "Parameters: $PARAM"

echo "" >$FNCFG
#echo "bindaddr 192.168.1.25" >>$FNCFG
echo "sv_name Official InfClass Server $(expr $NUM)" >>$FNCFG
echo "sv_port $PORT" >>$FNCFG
echo "sv_rcon_password $3" >>$FNCFG
echo "sv_rcon_mod_password $4" >>$FNCFG
echo "sv_rounds_per_map 5" >>$FNCFG
echo "mod_command ban 1" >>$FNCFG
echo "mod_command unban 1" >>$FNCFG
echo "mod_command bans 1" >>$FNCFG
echo "mod_command kick 1" >>$FNCFG
echo "mod_command status 1" >>$FNCFG
echo "mod_command mod_status 1" >>$FNCFG
echo "mod_command inf_set_class 1" >>$FNCFG
echo "mod_command sv_map 1" >>$FNCFG
echo "mod_command mute 1" >>$FNCFG
echo "mod_command unmute 1" >>$FNCFG
echo "mod_command set_team 1" >>$FNCFG
echo "inf_add_sqlserver w off_infc tw $5 $6 $MYSQL_HOST 3306 1" >>$FNCFG
echo "inf_challenge 1" >>$FNCFG
echo 'add_vote "Next map" "skip_map"' >>$FNCFG

echo 'sv_max_clients 63' >>$FNCFG
echo 'sv_distconnlimit 3' >>$FNCFG
echo 'sv_distconnlimit_time 10' >>$FNCFG



#echo "run $PARAM" >$FNGDB
#echo "backtrace" >>$FNGDB
#echo "quit" >>$FNGDB

#gdb -x $FNGDB ./infclass_srv_sql_d  >$FNLOG 2>&1
#$DIR/infclass_srv_sql -f $FNCFG  >$FNLOG 2>&1
$DIR/infclass_srv_sql -f $FNCFG
